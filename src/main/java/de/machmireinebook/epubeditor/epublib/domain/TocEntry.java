package de.machmireinebook.epubeditor.epublib.domain;

import java.io.Serial;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ObjectUtils;

import org.jdom2.Document;

import de.machmireinebook.epubeditor.epublib.resource.Resource;
import de.machmireinebook.epubeditor.epublib.resource.TitledResourceReference;

import lombok.Getter;
import lombok.Setter;

/**
 * An item in the Table of Contents.
 *
 * S - type of childs
 */
@Getter
@Setter
public class TocEntry extends TitledResourceReference<Document> implements Cloneable {

	/**
	 * 
	 */
	@Serial
	private static final long serialVersionUID = 5787958246077042456L;
	private TocEntry parent;
	private List<TocEntry> children;
    private String reference;
	private String tocNumber;
	private int level;

	public TocEntry() {
		this(null, null, null);
	}
	
	public TocEntry(String name, Resource<Document> resource) {
		this(name, resource, null);
	}
	
	public TocEntry(String name, Resource<Document> resource, String fragmentId) {
		this(name, resource, fragmentId, new ArrayList<>());
	}
	
	public TocEntry(String title, Resource<Document> resource, String fragmentId, List<TocEntry> children) {
		super(resource, title, fragmentId);
		this.children = children;
	}

	public boolean hasChildren() {
        return !children.isEmpty();
    }

	public List<TocEntry> getChildren() {
		return children;
	}

	public void setChildren(List<TocEntry> children) {
		this.children = children;
	}

	public void addChild(TocEntry child) {
		children.add(child);
		child.setParent(this);
	}

	@Override
	public TocEntry clone() {
		TocEntry newTocEntry = null;
		try {
			newTocEntry = (TocEntry) super.clone();
			newTocEntry.setChildren(ObjectUtils.clone(getChildren()));
		}
		catch (CloneNotSupportedException e) {
			//
		}
		return newTocEntry;
	}
}
