package de.machmireinebook.epubeditor.fonts;

import com.helger.css.decl.CSSDeclaration;
import com.helger.css.decl.CSSExpression;
import com.helger.css.decl.CSSFontFaceRule;

import de.machmireinebook.epubeditor.epublib.resource.FontResource;

public class FontCssGenerator {
    public static CSSFontFaceRule of(FontResource fontResource, AddFontToCssDialog.AddFontDialogResult params) {
        /*
        @font-face {
    font-family: "Crimson";
	font-weight:normal;
	font-style:normal;
    src: url('../Fonts/CrimsonText-Roman.ttf');
}
         */
        CSSFontFaceRule rule = new CSSFontFaceRule();
        rule.addDeclaration(new CSSDeclaration("font-family", CSSExpression.createString(params.getFontFamily())));
        rule.addDeclaration(new CSSDeclaration("font-weight", CSSExpression.createSimple(params.getFontWeight())));
        rule.addDeclaration(new CSSDeclaration("font-style", CSSExpression.createSimple(params.getFontStyle())));
        String url = params.getCssFile().relativize(fontResource);
        rule.addDeclaration("src",  CSSExpression.createURI (url), false);
        return rule;
    }
}
