package de.machmireinebook.epubeditor.epublib.resource;

import javafx.util.StringConverter;

import org.apache.commons.lang3.StringUtils;

public class ResourceStringConverter<T extends Resource<?>> extends StringConverter<T> {
    private final T resource;

    public ResourceStringConverter(T resource) {
        this.resource = resource;
    }

    @Override
    public String toString(T resource) {
        return resource.getFileName();
    }

    @Override
    public T fromString(String cellContent) {
        int index = StringUtils.lastIndexOf(resource.getHref(), "/");
        String fileName = resource.getHref();
        if (index > -1)
        {
            fileName = resource.getHref().substring(index + 1);
        }
        resource.setHref(resource.getHref().replace(fileName, cellContent));
        return resource;
    }
}
