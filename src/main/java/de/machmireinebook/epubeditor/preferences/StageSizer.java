package de.machmireinebook.epubeditor.preferences;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * User: Michail Jungierek
 * Date: 13.05.2018
 * Time: 11:34
 */
@Data
@Slf4j
public class StageSizer
{
    private static final int MINIMUM_VISIBLE_WIDTH = 100;
    private static final int MINIMUM_VISIBLE_HEIGHT = 50;
    private static final int MARGIN = 0;

    private Boolean maximized;
    private Double x;
    private Double y;
    private Double width;
    private Double height;

    public void setStage(Stage stage) {
        // First, restore the size and position of the stage.
        resizeAndPosition(stage);
        // If the stage is not visible in any of the current screens, relocate it at the primary screen.
        if (isWindowIsOutOfBounds(stage)) {
            log.debug("WindowIsOutOfBounds");
            moveToPrimaryScreen(stage);
        }
        // And now watch the stage to keep the properties updated.
        watchStage(stage);
    }

    private void resizeAndPosition(Stage stage) {
        log.debug("resize stage, x: {}, y: {}, width: {}, height: {}, maximized: {}", x, y, width, height, maximized);
        if (getX() != null) {
            stage.setX(getX());
        }
        if (getY() != null) {
            stage.setY(getY());
        }
        if (getWidth() != null) {
            stage.setWidth(getWidth());
        }
        if (getHeight() != null) {
            stage.setHeight(getHeight());
        }
        if (getMaximized() != null) {
            stage.setMaximized(getMaximized());
        }
    }

    private boolean isWindowIsOutOfBounds(Stage stage) {
        boolean windowIsOutOfBounds = true;
        for (Screen screen : Screen.getScreens()) {
            Rectangle2D bounds = screen.getVisualBounds();
            if (bounds.getMinX() <= stage.getX()
                    && stage.getX() + MINIMUM_VISIBLE_WIDTH < bounds.getMaxX()
                    && bounds.getMinY() <= stage.getY()
                    && stage.getY() + MINIMUM_VISIBLE_HEIGHT < bounds.getMaxY()) {
                windowIsOutOfBounds = false;
                break;
            }
        }
        return windowIsOutOfBounds;
    }

    private void moveToPrimaryScreen(Stage stage) {
        Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
        stage.setX(bounds.getMinX() + MARGIN);
        stage.setY(bounds.getMinY() + MARGIN);
        if (width <= MINIMUM_VISIBLE_WIDTH || width > bounds.getWidth() - MARGIN * 2) {
            stage.setWidth(bounds.getWidth() - MARGIN * 2);
        }
        if (height <= MINIMUM_VISIBLE_HEIGHT || height > bounds.getHeight() - MARGIN * 2) {
            stage.setHeight(bounds.getHeight() - MARGIN * 2);
        }
    }

    private void watchStage(Stage stage) {
        // Get the current values
        setX(stage.getX());
        setY(stage.getY());
        setWidth(stage.getWidth());
        setHeight(stage.getHeight());
        setMaximized(stage.isMaximized());
        // Watch for future changes
        stage.xProperty().addListener((observable, old, x) -> {
            log.debug("stage x value changed: {}", x);
            setX(x.doubleValue());
        });
        stage.yProperty().addListener((observable, old, y) -> {
            log.debug("stage y value changed: {}", y);
            setY(y.doubleValue());
        });
        stage.widthProperty().addListener((observable, old, width) -> setWidth(width.doubleValue()));
        stage.heightProperty().addListener((observable, old, height) -> setHeight(height.doubleValue()));
        stage.maximizedProperty().addListener((observable, old, maximized) -> setMaximized(maximized));
    }
}
