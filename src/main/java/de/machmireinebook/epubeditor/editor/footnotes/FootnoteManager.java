package de.machmireinebook.epubeditor.editor.footnotes;

import org.jdom2.Document;

import de.machmireinebook.epubeditor.editor.XhtmlRichTextCodeEditor;
import de.machmireinebook.epubeditor.epublib.resource.XHTMLResource;

public class FootnoteManager {
    private final XHTMLResource resource;
    private final XhtmlRichTextCodeEditor editor;
    private int numberFootnotes;

    public FootnoteManager(XhtmlRichTextCodeEditor editor, XHTMLResource resource) {
        this.editor = editor;
        this.resource = resource;
    }

    public void addFootnote() {
        int newNumber = numberFootnotes++;
        //insert new footnote
        String newFootnoteReference = "<a href=\"#footnote-ref-" + newNumber + "\" id=\"footnote-text-" + newNumber + "\" class=\"footnote-reference\">" + newNumber +")</a>";
        Integer cursorPosition = editor.getAbsoluteCursorPosition();
        editor.insertAt(cursorPosition, newFootnoteReference);

        Document document = resource.asNativeFormat();
        // if a footnote area not exists, create one

        //insert new footnote text
        //renumber all footnotes, that is in correct order
    }
}
