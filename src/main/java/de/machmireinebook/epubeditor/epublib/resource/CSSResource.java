package de.machmireinebook.epubeditor.epublib.resource;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import com.helger.css.ECSSVersion;
import com.helger.css.decl.CascadingStyleSheet;
import com.helger.css.reader.CSSReader;
import com.helger.css.writer.CSSWriter;

import de.machmireinebook.epubeditor.epublib.domain.MediaType;

import lombok.extern.slf4j.Slf4j;

/**
 * User: mjungierek
 * Date: 01.09.2014
 * Time: 19:12
 */
@Slf4j
public class CSSResource extends Resource<CascadingStyleSheet> implements TextResource
{
    public CSSResource()
    {
    }

    public CSSResource(String href)
    {
        super(href);
    }

    public CSSResource(byte[] data, String href)
    {
        super(data, href);
    }

    public CSSResource(String id, byte[] data, String href)
    {
        super(id, data, href, MediaType.CSS);
    }

    public CSSResource(byte[] data, String href, MediaType mediaType)
    {
        super(data, href, mediaType);
    }

    @Override
    public CascadingStyleSheet asNativeFormat()
    {
        String cssString = asString();
        return CSSReader.readFromString(cssString, ECSSVersion.CSS30);
    }

    @Override
    public String asString() {
        try {
            return new String(getData(), getInputEncoding());
        }
        catch (UnsupportedEncodingException e) {
            //should not happens
            return null;
        }
    }

    @Override
    public void setData(CascadingStyleSheet css) {
        CSSWriter writer = new CSSWriter();
        writer.setHeaderText(null);
        setData(writer.getCSSAsString(css).getBytes(StandardCharsets.UTF_8));
    }

}
