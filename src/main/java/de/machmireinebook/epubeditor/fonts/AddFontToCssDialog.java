package de.machmireinebook.epubeditor.fonts;

import java.util.Arrays;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

import de.machmireinebook.epubeditor.epublib.domain.Book;
import de.machmireinebook.epubeditor.epublib.resource.CSSResource;
import de.machmireinebook.epubeditor.epublib.resource.Resource;
import de.machmireinebook.epubeditor.javafx.FXUtils;

import lombok.AllArgsConstructor;
import lombok.Data;

public class AddFontToCssDialog extends Dialog<AddFontToCssDialog.AddFontDialogResult> {

    private final Book book;

    @Data
    @AllArgsConstructor
    public static class AddFontDialogResult {
        private Resource<?> cssFile;
        private String fontFamily;
        private String fontWeight;
        private String fontStyle;
    }

    public AddFontToCssDialog(Book book) {
        this.book = book;

        setTitle("Add font to css");
        setGraphic(FXUtils.getIcon("/icons/choose_font_96px.png", 64));

        final GridPane content = new GridPane();
        content.setPadding(new Insets(10d, 10d, 0d, 20d));
        content.setHgap(10);
        content.setVgap(10);

        content.add(new Label("Css file"), 0, 0);
        ComboBox<CSSResource> cbCssFiles = new ComboBox<>();
        cbCssFiles.setPrefWidth(100);
        ObservableList<CSSResource> cssFiles = book.getResources().getCssResources();
        cbCssFiles.setItems(cssFiles);
        cbCssFiles.getSelectionModel().select(0);
        content.add(cbCssFiles, 1, 0);
        cbCssFiles.setVisibleRowCount(cssFiles.size());
        GridPane.setHgrow(cbCssFiles, Priority.ALWAYS);

        content.add(new Label("Font family"), 0, 1);
        TextField textFieldFontFamily = new TextField();
        textFieldFontFamily.setPrefWidth(100);
        content.add(textFieldFontFamily, 1, 1);
        GridPane.setHgrow(cbCssFiles, Priority.ALWAYS);

        content.add(new Label("Font weight"), 0, 2);
        ComboBox<String> cbFontWeight = new ComboBox<>();
        cbFontWeight.setPrefWidth(100);
        cbFontWeight.setItems(FXCollections.observableList(Arrays.asList("normal", "bold", "100", "200", "300", "400", "500", "600", "700", "800", "900")));
        cbFontWeight.getSelectionModel().select(0);
        content.add(cbFontWeight, 1, 2);
        cbFontWeight.setVisibleRowCount(11);
        GridPane.setHgrow(cbFontWeight, Priority.ALWAYS);

        content.add(new Label("Font style"), 0, 3);
        ComboBox<String> cbFontStyle = new ComboBox<>();
        cbFontStyle.setPrefWidth(100);
        cbFontStyle.setItems(FXCollections.observableList(Arrays.asList("normal", "italic")));
        cbFontStyle.getSelectionModel().select(0);
        content.add(cbFontStyle, 1, 3);
        cbFontStyle.setVisibleRowCount(4);
        GridPane.setHgrow(cbFontStyle, Priority.ALWAYS);

        setResultConverter(dialogButton -> {
            if (dialogButton == ButtonType.FINISH) {
                return new AddFontDialogResult(cbCssFiles.getSelectionModel().getSelectedItem(),
                        textFieldFontFamily.getText(),
                        cbFontWeight.getSelectionModel().getSelectedItem(),
                        cbFontStyle.getSelectionModel().getSelectedItem());
            }
            return null;
        });

        setResizable(false);

        getDialogPane().setContent(content);
        getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.FINISH);
    }
}
