package de.machmireinebook.epubeditor.editor;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.IntFunction;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;

import org.apache.log4j.Logger;

import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.languagetool.rules.RuleMatch;

import de.machmireinebook.epubeditor.editor.regex.CssRegexLexer;
import de.machmireinebook.epubeditor.editor.regex.RegexToken;
import de.machmireinebook.epubeditor.epublib.domain.MediaType;

import lombok.extern.slf4j.Slf4j;

/**
 * User: mjungierek
 * Date: 24.12.2014
 * Time: 01:11
 */
@Slf4j
public class CssRichTextCodeEditor extends AbstractRichTextCodeEditor
{
    private static final Logger logger = Logger.getLogger(CssRichTextCodeEditor.class);
    private final CssRegexLexer cssRegexLexer = new CssRegexLexer();

    public CssRichTextCodeEditor()
    {
        super();

        String stylesheet = AbstractRichTextCodeEditor.class.getResource("/editor-css/css.css").toExternalForm();
        addStyleSheet(stylesheet);
        setWrapText(true);
    }

    protected void createParagraphGraphicFactory() {
        IntFunction<String> format = (digits -> " %" + digits + "d ");
        IntFunction<Node> numberFactory = LineNumberFactory.get(codeArea, format);
        IntFunction<Node> colorFactory = ColorFactory.get(codeArea);
        IntFunction<Node> graphicFactory = line -> {
            HBox hbox = new HBox(
                    numberFactory.apply(line),
                    colorFactory.apply(line));
            hbox.setBackground(DEFAULT_BACKGROUND);
            hbox.setPadding(DEFAULT_INSETS);

            hbox.setAlignment(Pos.CENTER_LEFT);
            return hbox;
        };
        codeArea.setParagraphGraphicFactory(graphicFactory);
    }


    @Override
    public MediaType getMediaType()
    {
        return MediaType.CSS;
    }

    @Override
    public List<RuleMatch> spellCheck() {
        return Collections.emptyList();
    }

    @Override
    public void applySpellCheckResults(List<RuleMatch> matches) {
    }

    @Override
    public void initContextMenu() {
        ContextMenu contextMenuCSS = new ContextMenu();
        contextMenuCSS.getStyleClass().add("context-menu");
        contextMenuCSS.setAutoFix(true);
        contextMenuCSS.setAutoHide(true);
        MenuItem formatCSSOneLineItem = new MenuItem("Format styles in one line");
        formatCSSOneLineItem.setOnAction(e -> beautifyCSS("one_line"));
        contextMenuCSS.getItems().add(formatCSSOneLineItem);

        MenuItem formatCSSMultipleLinesItem = new MenuItem("Format styles in multiple lines");
        formatCSSMultipleLinesItem.setOnAction(e -> beautifyCSS("multiple_lines"));
        contextMenuCSS.getItems().add(formatCSSMultipleLinesItem);
        setContextMenu(contextMenuCSS);
    }

    protected StyleSpans<Collection<String>> computeHighlighting(String text)
    {
        StyleSpansBuilder<Collection<String>> spansBuilder = new StyleSpansBuilder<>();
        cssRegexLexer.setContent(text);

        int lastKwEnd = 0;
        RegexToken token;

        token = cssRegexLexer.nextToken();
        while (token != null) {
            boolean previousTokenIsFont = false;
            spansBuilder.add(Collections.emptyList(), token.getStart() - lastKwEnd);
            Set<String> style = Collections.singleton(token.getCode().toLowerCase());
            int length = token.getEnd() - token.getStart();
            if ("font".equals(token.getText())) {
                previousTokenIsFont = true;
            } else {
                spansBuilder.add(style, length);
            }
            lastKwEnd = token.getEnd();
            token = cssRegexLexer.nextToken();
            if (previousTokenIsFont) {
                if (token != null && token.getText() != null && token.getText().startsWith("-")) {
                    //"font" is not alone but part of another larger splitted token, #
                    // in this case this token gets the style of the next token
                    Set<String> nextStyle = Collections.singleton(token.getCode().toLowerCase());
                    spansBuilder.add(nextStyle, length);
                } else {
                    spansBuilder.add(style, length);
                }
            }
        }

        spansBuilder.add(Collections.emptyList(), text.length() - lastKwEnd);
        return spansBuilder.create();
    }

    private void beautifyCSS(String type) {
    }

}
