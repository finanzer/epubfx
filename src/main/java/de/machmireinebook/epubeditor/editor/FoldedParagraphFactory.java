package de.machmireinebook.epubeditor.editor;

import java.util.function.IntFunction;

import javafx.scene.Node;
import javafx.scene.text.Text;

import org.fxmisc.richtext.GenericStyledArea;
import org.fxmisc.richtext.model.Paragraph;
import org.reactfx.collection.LiveList;

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon;
import de.jensd.fx.glyphs.fontawesome.utils.FontAwesomeIconFactory;

import lombok.extern.slf4j.Slf4j;

/**
 * To customize appearance, use {@code .lineno} style class in CSS stylesheets.
 */
@Slf4j
public class FoldedParagraphFactory implements IntFunction<Node> {

    public static IntFunction<Node> get(
            GenericStyledArea<?, ?, ?> area) {
        return new FoldedParagraphFactory(area);
    }

    private final LiveList<? extends Paragraph<?, ?, ?>> paragraphs;

    private FoldedParagraphFactory(GenericStyledArea<?, ?, ?> area) {
        paragraphs = area.getParagraphs();
    }

    @Override
    public Node apply(int idx) {
        String lineText = paragraphs.get(idx).getText();
        Text text = FontAwesomeIconFactory.get().createIcon(FontAwesomeIcon.CARET_DOWN);
        return text;
    }

}
