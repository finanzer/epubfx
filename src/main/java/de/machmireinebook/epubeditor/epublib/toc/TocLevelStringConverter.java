package de.machmireinebook.epubeditor.epublib.toc;

import javafx.util.StringConverter;

/**
 * <p>{@link StringConverter} implementation for {@link String} values.</p>
 * @since JavaFX 2.1
 */
public class TocLevelStringConverter extends StringConverter<Integer> {
    /** {@inheritDoc} */
    @SuppressWarnings("StringConcatenationMissingWhitespace")
    @Override public String toString(Integer value) {
        return (value != null) ? "h" + value : "p";
    }

    /** {@inheritDoc} */
    @Override public Integer fromString(String value) {
        return TocGenerator.getLevel(value);
    }
}

